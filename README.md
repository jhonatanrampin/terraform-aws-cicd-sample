## GitLab Settings

### CI/CD Visibility

    Settings -> General -> Visibility, project features, permissions -> CI/CD (Only Project Members)
    Settings -> CI/CD -> General Pipelines -> Untick `Public pipelines`

### Protected Branches

    Settings -> Repository -> Protected branches:

      * Branch: *-release

      * Allowed to merge: Maintainers

      * Allowed to push: Maintainers

      * Require approval from code owners: Enabled

### Protected Tags

    Settings -> Repository -> Protected tags:

      * Branch: *-release

      * Allowed to merge: Maintainers

