module "testing" {
  source = "git::https://gitlab.com/jhonatanrampin/terraform-modules.git?ref=0.0.1"
}

output "main_output" {
  value = module.testing.testing_gitlab_module
}

