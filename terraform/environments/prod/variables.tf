variable "environment" {
  description = "The environment name. Used to name and tag resources"
  type        = string
  default     = "prod"
}
