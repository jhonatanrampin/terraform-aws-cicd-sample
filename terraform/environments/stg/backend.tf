terraform {
  backend "s3" {
    bucket  = "jr-labs-2021"
    key     = "stg/terraform.state"
    region  = "ap-southeast-2"
    encrypt = true
  }
}
